/**
 * @format
 */

import {AppRegistry} from 'react-native';
import Cat from './modules/registration/view/registration';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => Cat);
