import React, { PureComponent } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Button,
  Image,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

const styles = StyleSheet.create({
  container: {
    display: "flex",
    alignSelf: "center",
    marginTop: 293,
    marginBottom: 292,
    marginLeft: 33,
    marginRight: 32
  },
  logo: {
    width: 310,
    height: 82,
    backgroundColor: '#FFFFFF'
    
  },
});

class DisplayAnImage extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Image
          style={styles.logo}
          source={{uri: 'https://public-v2links.adobecc.com/7938e621-db25-449f-4b27-4b22d118363f/component?params=component_id%3A081fc18e-a128-4576-aaa6-bae0ff62a7d6&params=version%3A0&token=1595694817_da39a3ee_3cfe5d9a3204b89d77c8679293b029bea4fe0058&api_key=CometServer1'}}
        />
      </View>
    );
  }
}

export default DisplayAnImage;



  
