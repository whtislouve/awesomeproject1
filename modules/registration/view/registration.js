import React, { PureComponent } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Button,
  Image,
  TextInput,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

const styles = StyleSheet.create({
  container: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center"
  },
  reg_word: {
    marginTop: 15,
    //marginLeft: 119,
    //marginRight: 118,
    fontFamily: "SF UI Display",
    fontStyle: "normal",
    fontSize: 20
  },
  text_input: {
    width: 343,
    height: 45,
    marginTop: 14,
    
    borderColor: 'gray',
    borderWidth: 1
  },
  enter_button: {
    width: 343,
    height: 40,
    marginTop: 14,
    
  },
  id_trainer: {
    display: "flex",
    alignSelf: "flex-start",
    marginLeft: 16

  }

  
});

class DisplayRegistration extends React.Component {
    render(){
        return (
            <View style = {styles.container}>
              <Text style = {styles.reg_word}>Регистрация</Text>

              <View style = {styles.id_trainer}>
                <Text >Код тренажера</Text>
              </View>
              
              <TextInput style = {styles.text_input}></TextInput>
              <TextInput style = {styles.text_input}></TextInput>
              <View style = {styles.enter_button}>  
                <Button title = "Войти" onPress={() => {}}> </Button>
              </View>
            </View>
        );
    }
}

export default DisplayRegistration